#!/usr/bin/env node
const fs = require('fs');
const path = require('path');

const yargsLinkServiceCommand = (yargs) => {
  return yargs.command(
    'link-service',
    "Create an 'nginx' service symlink.",
    (yargs) => {
      yargs.option('config', {
        type: 'string',
        describe: 'The path to the Nginx configuration file.',
        requiresArg: true,
      });
    },
    (argv) => {
      const serviceTemplate = fs.readFileSync(
        path.join(__dirname, '../nginx.service')
      );
      const serviceStartScriptPath = path.join(
        __dirname,
        '../service-start.sh'
      );

      const service = serviceTemplate
        .toString()
        .replace('$SERVICE_START_SCRIPT', path.resolve(serviceStartScriptPath))
        .replace('$CONFIGURATION_PATH', path.resolve(argv.config));

      fs.writeFileSync('/etc/systemd/system/nginx.service', service);
    }
  );
};

const yargs = require('yargs')
  .scriptName('nginx-utilities')
  .usage('$0 <command> [arguments]');

yargsLinkServiceCommand(yargs);

yargs.help().argv;
